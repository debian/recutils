#!/bin/bash

csv2rec -t movies -e movies.csv > movies.rec

# The -e option omits empty fields.
#
# ---------------------------------------------
# A conversion without -e would look like this:
# ---------------------------------------------
#
# ...
# Amount_of_Media: 1
# Rank: 
# Rating: 7
# Region: 
# Series: 
# Subtitles: 
# Video_file: 
# Video_format: divx
# ...
#
#
# --------------------
# Using the -e option:
# --------------------
#
# ...
# Amount_of_Media: 1
# Rating: 7
# Video_format: divx
# ...
#
